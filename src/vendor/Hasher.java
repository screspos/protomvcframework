package vendor;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import models.Usuario;

/**
 * Clase utilizada para guardar y recuperar las contraseņas de forma segura.
 * 
 * 
 * @author saulc
 *
 */
public class Hasher {
	public static String encript(String s, byte[] salt) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			md.update(salt);
			byte[] message = md.digest(s.getBytes());
			StringBuilder sb= new StringBuilder();
			for (int i = 0; i < message.length; i++) {
				sb.append((Integer.toString((message[i] & 0xff) + 0x100, 16).substring(1)));
			}
			String hashed = sb.toString();
			while (hashed.length() < 32) {
				hashed = "0" + hashed;
			}
			return hashed;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException();
		}
	}
	
	public static byte[] getSalt() {
		SecureRandom sr = new SecureRandom();
		byte[] salt = new byte[16];
		sr.nextBytes(salt);
		return salt;
	}
	
	public static String saltToString(byte[] salt) {
		return new String(salt);
	}
	
	public static String encodeSalt(byte[] salt) {
		String temp = "";
		for (int i = 0; i < salt.length; i++) {
			temp += (char) (salt[i] + 127);
		}
		return temp;
	}
	
	public static byte[] decodeSalt(String salt) {
		byte[] arr = new byte[16];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = (byte) (salt.charAt(i) - 127);
		}
		return arr;
	}
	
	public static boolean compare(String password, Usuario usuario) {
		return encript(password,usuario.getSalt()).equals(usuario.getHashedPassword());
	}
	
	public static String customSaltToString(byte[] salt) {
		String temp = "";
		for(int i=0; i< salt.length; i++) {
			temp += Integer.toString(salt[i] + 127);
			temp += (i==temp.length()-1)?"":",";
		}
		return temp;
	}
	
	public static byte[] customStringToSalt(String salt) {
		String[] arr = salt.split(",");
		byte[] temp = new byte[arr.length];
		for(int i= 0; i< temp.length; i++) {
			temp[i] = (byte) ((byte) Integer.parseInt(arr[i]) - 127);
		}
		return temp; 
	}
}