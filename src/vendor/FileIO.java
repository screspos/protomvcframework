package vendor;

public interface FileIO<T> {
	
	public T leer(String ruta);
	
	public void escribir(T contenido, String ruta);
}
