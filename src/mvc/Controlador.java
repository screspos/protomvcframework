package mvc;

/**
 * Clase abstracta que sirve como base para los controladores en el patr�n MVC.
 * @param <T> Tipo gen�rico que extiende de la clase Modelo.
 */
public abstract class Controlador<T extends Modelo> {
    
    /** El modelo asociado a este controlador. */
    protected T modelo;
    
    /** La vista asociada a este controlador. */
    protected Vista<T> vista;

    /**
     * Constructor que inicializa el controlador con un modelo dado.
     *
     * @param modelo El modelo que se asociar� a este controlador.
     */
    public Controlador(T modelo) {
        setModelo(modelo);
    }

    /**
     * Obtiene el modelo asociado a este controlador.
     *
     * @return El modelo asociado a este controlador.
     */
    public Modelo getModelo() {
        return modelo;
    }

    /**
     * Establece el modelo asociado a este controlador.
     * Adem�s, asocia este controlador al modelo.
     *
     * @param modelo El modelo que se asociar� a este controlador.
     */
    public void setModelo(T modelo) {
        this.modelo = modelo;
        this.modelo.setControlador(this);
    }

    /**
     * Obtiene la vista asociada a este controlador.
     *
     * @return La vista asociada a este controlador.
     */
    public Vista<T> getVista() {
        return vista;
    }

    /**
     * Establece la vista asociada a este controlador.
     *
     * @param vista La vista que se asociar� a este controlador.
     */
    public void setVista(Vista<T> vista) {
        this.vista = vista;
    }
}
