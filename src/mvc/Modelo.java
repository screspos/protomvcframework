package mvc;

import java.io.Serializable;

/**
 * Clase abstracta que sirve como base para los modelos en el patrón MVC.
 * Implementa la interfaz Serializable para permitir la serialización de objetos.
 */
public abstract class Modelo implements Serializable {
    /**
     * Identificador de versión para la serialización.
     */
    private static final long serialVersionUID = -3915307089004801069L;

    // Referencia al controlador asociado a este modelo.
    private Controlador<Modelo> controlador;

    // Referencia a la vista asociada a este modelo.
    private Vista<Modelo> vista;

    /**
     * Establece la vista asociada a este modelo.
     *
     * @param vista La vista que se asociará a este modelo.
     */
    public void setVista(Vista vista) {
        this.vista = vista;
    }

    /**
     * Establece el controlador asociado a este modelo.
     *
     * @param controlador El controlador que se asociará a este modelo.
     */
    public void setControlador(Controlador controlador) {
        this.controlador = controlador;
    }

    /**
     * Obtiene la vista asociada a este modelo.
     *
     * @return La vista asociada a este modelo.
     */
    public Vista getVista() {
        return vista;
    }

    /**
     * Obtiene el controlador asociado a este modelo.
     *
     * @return El controlador asociado a este modelo.
     */
    public Controlador getControlador() {
        return controlador;
    }
}
