package mvc;

import javax.swing.JPanel;
/**
 * Clase base para la vista del MVC
 * 
 * Hereda de la clase JPanel para poder trabajar con varias vistas dentro de un frame.
 *
 * @param <T>
 */
public abstract class Vista<T extends Modelo> extends JPanel {

	private static final long serialVersionUID = 201062851051957085L;
	
	protected Controlador<T> controlador;
	protected T modelo;
	
	public Vista() {
		
	}
	
	public Vista(T modelo, Controlador<T> controlador) {
		this.modelo = modelo;
		this.modelo.setVista(this);
		this.controlador = controlador;
		this.controlador.setModelo(modelo);
		this.controlador.setVista(this);
	}
	
	public Modelo getModelo() {
		return modelo;
	}
	
	public void setModelo(T modelo) {
		this.modelo = modelo; 
	}
	
	
	public Controlador<T> getControlador() {
		return controlador;
	}
	
	public void setControlador(Controlador<T> controlador) {
		this.controlador = controlador;
	}
}
