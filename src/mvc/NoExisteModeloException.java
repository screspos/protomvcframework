package mvc;

public class NoExisteModeloException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoExisteModeloException() {
		super("No existe ning�n modelo en el que basar esta clase");
	}
}
