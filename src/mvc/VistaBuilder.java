package mvc;
/**
 * Clase base para el constructor de vistas.
 *
 * @param <T>
 */
public abstract class VistaBuilder<T extends Modelo> {
	protected Vista<T> vista;
	
	public Vista<T> create() {
		return vista;
	}
	
	protected abstract VistaBuilder<T> addModelo(T modelo);
	
	protected abstract VistaBuilder<T> addControlador(Controlador<T> controlador);
	
	public  Vista<T> build(T modelo, Controlador<T> controlador) {
		return addModelo(modelo).addControlador(controlador).create();
	}
}