package models;

import mvc.Modelo;
import vendor.Hasher;
/**
 * Clase modelo que representa un usuario de la apliacaci�n,
 * que hereda del Modelo del paquete mvc
 * 
 *
 */
public class Usuario extends Modelo {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1186585213940296750L;
	/**
	 * Nombre del usuario
	 */
	private String name;
	/**
	 * La contrase�a encriptada
	 */
	private String hashedPassword;
	/**
	 * La sal que utilizamos para a�adir seguridad a la contrase�a
	 */
	private byte[] salt;
	/**
	 * La contrase�a en texto plano, que NO queremos serializar.
	 */
	private transient String password;
	
	public Usuario() {
		name="";
	}
	
	public Usuario(String name, String password) {
		this.name = name;
		this.password = password;
		setHashedPassword(password);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHashedPassword() {
		return hashedPassword;
	}
	/**
	 * Crea una sal aleatoria exclusiva para este usuario, y
	 * encripta la contrase�a concatenando la contrase�a a la
	 * sal generada.
	 * 
	 * 
	 * @param password
	 */
	public void setHashedPassword(String password) {
		salt = Hasher.getSalt();
		hashedPassword = Hasher.encript(password, salt);
	}
	public byte[] getSalt() {
		return salt;
	}
	 
	public String getSaltToString() {
		return Hasher.saltToString(salt);
	}
	
	public String encodeSalt() {
		return Hasher.customSaltToString(salt);
	}
	
	public void decodeSalt(String s) {
		salt = Hasher.customStringToSalt(s);
	}
	 
	
	public boolean equals(String password) {
		return Hasher.compare(password, this);
	}
	
	@Override
	public String toString() {
		return name + ";" + hashedPassword + ";" + Hasher.customSaltToString(salt);
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}