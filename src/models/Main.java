package models;

import main.App;
import mvc.Modelo;
/**
 * Modelo para la ventana principal de la aplicación.
 * 
 *
 */
public class Main extends Modelo {
	/**
	 * Identificador de la clase que viene de la interfaz Serializable (sugerencia de Java)
	 */
	private static final long serialVersionUID = -2700904448202279407L;
	/**
	 * El nombre de la aplicación.
	 * 
	 */
	public String nombre;
	
	public Main() {
		nombre = App.NOMBRE;
	}
}
