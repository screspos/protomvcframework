package usuario.views;

import models.Usuario;
import mvc.Vista;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import controllers.UsuarioController;

public class RegisterView extends Vista<Usuario>{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6448463692693991451L;
	private JTextField usuario, pass1, repitepass;
	private JLabel titulo, usuarioLabel, pass1Label, repitePassLabel, resultado;
	private JButton registrar;
	private GroupLayout groupLayout;
	private MouseAdapter actionRegistrar = new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			resultado.setText(((UsuarioController) controlador).registrar(usuario.getText(),pass1.getText(),repitepass.getText()));
		}
	};
	
	public RegisterView() {
		
		titulo = new JLabel("Registrar Usuario");
		titulo.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		usuarioLabel = new JLabel("Usuario:");
		usuarioLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		usuario = new JTextField();
		usuario.setFont(new Font("Tahoma", Font.PLAIN, 16));
		usuario.setColumns(10);
		
		pass1Label = new JLabel("Contrase\u00F1a:");
		pass1Label.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		pass1 = new JTextField();
		pass1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		pass1.setColumns(10);
		
		repitepass = new JTextField();
		repitepass.setFont(new Font("Tahoma", Font.PLAIN, 16));
		repitepass.setColumns(10);
		
		repitePassLabel = new JLabel("Repite contrase\u00F1a:");
		repitePassLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		registrar = new JButton("Registrar");
		registrar.addMouseListener(actionRegistrar);
		registrar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		resultado = new JLabel("");
		resultado.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		// Configuración del diseño con el Group Layout
		groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(52, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
									.addComponent(usuarioLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(pass1Label, GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE))
								.addComponent(repitePassLabel, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(45)
									.addComponent(titulo, GroupLayout.PREFERRED_SIZE, 168, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(100)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(pass1, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
										.addComponent(usuario, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(repitepass, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)))))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(266)
							.addComponent(registrar)))
					.addGap(35))
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addGap(38)
					.addComponent(resultado, GroupLayout.PREFERRED_SIZE, 365, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(47, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(31)
					.addComponent(titulo, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
					.addGap(33)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(6)
							.addComponent(usuarioLabel)
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(pass1Label, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(usuario, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(pass1, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)))
					.addGap(31)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(repitepass, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
						.addComponent(repitePassLabel, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
					.addGap(30)
					.addComponent(registrar)
					.addGap(9)
					.addComponent(resultado, GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
					.addContainerGap())
		);
		setLayout(groupLayout);
	}

	
	
}
