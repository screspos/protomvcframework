package usuario.views;

// Importaci�n de controladores y modelos necesarios
import controllers.UsuarioController;
import models.Usuario;

// Definici�n de la clase Views
public abstract class Views {
	
	// M�todo est�tico para obtener una instancia de LoginView
	public static LoginView login(Usuario u, UsuarioController uc) {
		// Construye y devuelve una instancia de LoginView utilizando LoginViewBuilder
		return (LoginView) new LoginViewBuilder().build(u, new UsuarioController(u));
	}
	
	// M�todo est�tico para obtener una instancia de RegisterView
	public static RegisterView register(Usuario u, UsuarioController uc) {
		// Construye y devuelve una instancia de RegisterView utilizando RegisterViewBuilder
		return (RegisterView) new RegisterViewBuilder().build(u, new UsuarioController(u));
	}
}
