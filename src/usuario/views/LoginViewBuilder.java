package usuario.views;

// Definici�n de la clase LoginViewBuilder que extiende de UsuarioViewBuilder
public class LoginViewBuilder extends UsuarioViewBuilder {

	// Constructor de la clase
	public LoginViewBuilder() {
		vista = new LoginView(); // Inicializaci�n de la vista
	}
}
