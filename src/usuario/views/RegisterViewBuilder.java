package usuario.views;

// Definici�n de la clase RegisterViewBuilder que extiende de UsuarioViewBuilder
public class RegisterViewBuilder extends UsuarioViewBuilder {
	
	// Constructor de la clase
	public RegisterViewBuilder() {
		// Inicializaci�n de la vista como una nueva instancia de RegisterView
		vista = new RegisterView();
	}
}
