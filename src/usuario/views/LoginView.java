package usuario.views;

// Importaciones necesarias para la interfaz gr�fica
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

// Importaciones relacionadas con el modelo y el controlador
import models.Usuario;
import controllers.UsuarioController;
import mvc.Controlador;
import mvc.Vista;

// Definici�n de la clase LoginView que extiende de Vista
public class LoginView extends Vista<Usuario> {

	// Atributos para los componentes de la interfaz gr�fica
	private JTextField usuario, password;
	private JLabel usuarioLabel, passwordLabel, titulo, resultado;
	private JButton validar;
	private GroupLayout groupLayout;
	private MouseAdapter login = new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			// Acci�n al hacer clic en el bot�n de validar
			resultado.setText(((UsuarioController) controlador).validar(usuario.getText(),password.getText()));
		}
	};

	// Constructor de la clase
	public LoginView() {
		// Inicializaci�n de los componentes de la interfaz
		usuario = new JTextField();
		usuario.setFont(new Font("Tahoma", Font.PLAIN, 16));
		usuario.setColumns(10);

		validar = new JButton("Validar");
		validar.addMouseListener(login);
		validar.setFont(new Font("Tahoma", Font.PLAIN, 16));

		password = new JTextField();
		password.setFont(new Font("Tahoma", Font.PLAIN, 16));
		password.setColumns(10);

		usuarioLabel = new JLabel("Usuario:");
		usuarioLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));

		passwordLabel = new JLabel("Contrase�a:");
		passwordLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));

		titulo = new JLabel("Validar Usuario");
		titulo.setFont(new Font("Tahoma", Font.PLAIN, 20));

		resultado = new JLabel("");
		resultado.setFont(new Font("Tahoma", Font.PLAIN, 16));

		// Configuraci�n del dise�o de la interfaz utilizando GroupLayout
		groupLayout = new GroupLayout(this); //Sirve para organizar componentes en un contenedor
		setLayout(groupLayout);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(141)
					.addComponent(titulo, GroupLayout.PREFERRED_SIZE, 156, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(153, Short.MAX_VALUE))
				.addGroup(GroupLayout.Alignment.LEADING, groupLayout.createSequentialGroup()
					.addGap(36)
					.addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
						.addComponent(usuarioLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(passwordLabel, GroupLayout.PREFERRED_SIZE, 81, Short.MAX_VALUE))
					.addGap(49)
					.addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(validar)
							.addContainerGap())
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(usuario, GroupLayout.DEFAULT_SIZE, 193, Short.MAX_VALUE)
								.addComponent(password, 188, 188, 188))
							.addGap(91))))
				.addGroup(GroupLayout.Alignment.LEADING, groupLayout.createSequentialGroup()
					.addGap(60)
					.addComponent(resultado, GroupLayout.PREFERRED_SIZE, 308, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(82, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(27)
					.addComponent(titulo)
					.addGap(29)
					.addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(usuarioLabel)
						.addComponent(usuario, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
					.addGap(60)
					.addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(passwordLabel)
						.addComponent(password, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
					.addGap(56)
					.addComponent(validar)
					.addGap(28)
					.addComponent(resultado, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(32, Short.MAX_VALUE))
		);
	}
}
