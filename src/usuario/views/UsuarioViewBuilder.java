package usuario.views;

// Importaciones necesarias para el funcionamiento de la clase
import models.Usuario;
import mvc.Controlador;
import mvc.Vista;
import mvc.VistaBuilder;

// Definici�n de la clase abstracta UsuarioViewBuilder que extiende de VistaBuilder para el modelo Usuario
public abstract class UsuarioViewBuilder extends VistaBuilder<Usuario> {

	// M�todo para agregar el modelo a la vista
	@Override
	protected VistaBuilder<Usuario> addModelo(Usuario modelo) {
		// Establece la vista en el modelo y el modelo en la vista
		modelo.setVista(vista);
		vista.setModelo(modelo);
		return this;
	}

	// M�todo para agregar el controlador a la vista
	@Override
	public VistaBuilder<Usuario> addControlador(Controlador<Usuario> controlador) {
		// Establece la vista en el controlador y el controlador en la vista
		controlador.setVista(vista);
		vista.setControlador(controlador);
		return this;
	}

}
