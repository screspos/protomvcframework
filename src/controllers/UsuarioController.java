package controllers;

import java.io.File;

import models.Usuario;
import mvc.Controlador;
import vendor.Hasher;
import vendor.Serializador;
/**
 * 
 * Controlador del usuario
 * 
 *
 */
public class UsuarioController extends Controlador<Usuario> {

	/**
	 * Constructor que hereda del constructor de la clase padre
	 * @param modelo
	 */
	public UsuarioController(Usuario modelo) {
		super(modelo);
	}
	/**
	 * Este m�todo controla que los datos introducidos son correctos,
	 * y si lo son, crea un nuevo usuario y lo guarda en un archivo serializado.
	 * 
	 * Se le llama desde el bot�n Registrar de la vista Register.
	 * 
	 * 
	 * @param usuario
	 * @param pass1
	 * @param pass2
	 * @return El mensaje que se mostrar� en la vista en funci�n del resultado
	 * de las comprobaciones y acciones realizadas en el m�todo.
	 */
	public String registrar(String usuario, String pass1, String pass2) {
		if (!pass1.equals(pass2)) {
			return "Las dos contrase�as no son iguales";
		}
		if  (pass1.length() < 8) {
			return "La contrase�a es demasiado corta";
		}
		if (!checkCharacters(pass1)) {
			return "La contrase�a debe tener un n�mero, una letra min�scula,"
					+ "una letra may�scula y un car�cter especial";
		}
		String ruta = usuario + ".txt";
		File f = new File(ruta);
		
		if (f.exists()) {
			return "El usuario con el nombre " + usuario + " ya existe.";
		};
		// Crea un nuevo usuario y guarda sus datos serializados en un archivo de texto.
		new UsuarioSerializer().escribir(new Usuario(usuario,pass2), ruta);
		
		return "El usuario" + usuario + " ha sido";
	}
	
	/**
	 * M�todo que comprueba si un usuario introducido existe, y si la contrase�a
	 * es igual a la contrase�a asociada a dicho usuario
	 * 
	 * @param usuario Nombre del usuario con el que se desea entrar en la aplicaci�n
	 * @param pass La contrase�a introducida
	 * @return El mensaje que se mostrar� en la vista en funci�n del resultado
	 * de las comprobaciones y acciones realizadas en el m�todo.
	 */
	public String validar(String usuario, String pass) {
		String ruta = usuario + ".txt";
		File f = new File(ruta);
		// Comprueba si existe un archivo con ese nombre de usuario
		if (!f.exists()) {
			return "El usuario " + usuario + " no existe";
		}
		// Creamos un objeto serializador para leer el Usuario guardado en el archivo
		Usuario u = new UsuarioSerializer().leer(ruta);
		// Si la contrase�a es correcta, da la bienvenida
		return (Hasher.compare(pass, u))? "Bienvenido": "Contrase�a incorrecta";
	}
	/**
	 * M�todo privado que se utiliza para comprobar si una nueva contrase�a
	 * es segura o no (Debe tener al menos una letra may�scula, una min�scula, un n�mero y
	 * un car�cter especial).
	 * 
	 * @param pass
	 * @return
	 */
	private boolean checkCharacters(String pass) {
		boolean mayus, minus, number, special;
		mayus = minus = number = special = false;
		for(int i= 0; i < pass.length(); i++) {
			if (Character.isDigit(pass.charAt(i))) {
				number = true;
			} else if (Character.isLetter(pass.charAt(i))) {
				if ((int) pass.charAt(i) < 92) {
					mayus = true;
				} else {
					minus = true;
				}
			} else {
				special = true;
			}
		}
		return mayus && minus && number && special;
	}
}
/**
 * La implementaci�n de un Serializador para objetos usuario,
 * que vamos a usar dentro de la clase UsuarioController para
 * serializar los objetos Usuario
 *
 *
 */
class UsuarioSerializer extends Serializador<Usuario> {
	
}
