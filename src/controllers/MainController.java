package controllers;

import models.Main;
import mvc.Controlador;
/**
 * Controlador del modelo principal.
 * 
 * Hereda del controlador gen�rico del paquete mvc,
 * con el tipo Main, que va a ser su modelo.
 * 
 * 
 * @author saulc
 *
 */
public class MainController extends Controlador<Main> {
	public MainController(Main modelo) {
		super(modelo);
	}

}
