package main;

import controllers.MainController;
import main.views.MainViewBuilder;
import models.Main;
import mvc.Controlador;
import mvc.Modelo;
import mvc.Vista;

/**
 * Clase que contiene la configuracion
 * de la aplicacion. Esta clase
 * implementa el patr�n de dise�o creacional
 * Singleton, que hace que s�lo pueda haber
 * una �nica instancia de esta clase en la aplicaci�n.
 * 
 * La clase contiene la �nica instancia de s� misma,
 * y el modelo, el controlador y la vista de la ventana principal.
 * 
 * 
 *
 */
public class App {
	// �nica instancia de la clase
	private static App instance = new App();
	// atributo para el modelo.
	private Modelo modelo;
	// Atributo para guardar el controlador.
	private Controlador controlador;
	// Atributo para guardar la visdta.
	private Vista vista;
	// Nombre de la aplicaci�n.
	public static final String NOMBRE = "Proto FrameWork MVC";
	
	/**
	 * Constructor privado de la clase, para que no se puedan crear instancias fuera
	 * de ella.
	 */
	private App() {
		// Crea el modelo principal.
		modelo = new Main();
		// Crea el controlador en base al modelo.
		controlador = new MainController((Main)modelo);
		// Crea la vista usando el Builder de la clase Main.
		vista = new MainViewBuilder().build((Main) modelo, controlador);
	}
	
	// Devuelve la instancia de la aplicaci�n.
	public static App getInstance() {
		return instance;
	}
}