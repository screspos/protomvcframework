package main;
/**
 * 
 * Clase que contiene el método main y que lanza la aplicación.
 *
 */
public class Flow {
	public static void main(String[] args) {
		// Guarda la instancia de la aplicación, inicializándola.
		App app = App.getInstance();	
	}
}
