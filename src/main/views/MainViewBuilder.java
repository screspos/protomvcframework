package main.views;

// Importaciones necesarias para el funcionamiento de la clase
import models.Main;
import mvc.Controlador;
import mvc.Vista;
import mvc.VistaBuilder;

// Definici�n de la clase MainViewBuilder que extiende de VistaBuilder
public class MainViewBuilder extends VistaBuilder<Main> {

	// Constructor de la clase
	public MainViewBuilder() {
		this.vista = new MainView(); // Inicializaci�n de la vista
	}
	
	// M�todo para agregar el modelo a la vista
	@Override
	protected VistaBuilder<Main> addModelo(Main modelo) {
		vista.setModelo(modelo); // Establecer el modelo en la vista
		modelo.setVista(vista); // Establecer la vista en el modelo
		return this; // Devolver una instancia de esta clase
	}

	// M�todo para agregar el controlador a la vista
	@Override
	public VistaBuilder<Main> addControlador(Controlador<Main> controlador) {
		vista.setControlador(controlador); // Establecer el controlador en la vista
		controlador.setVista(vista); // Establecer la vista en el controlador
		return this; // Devolver una instancia de esta clase
	}	
}
