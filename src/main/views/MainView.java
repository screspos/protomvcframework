package main.views;

// Importaciones necesarias para la interfaz gr�fica
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

// Importaciones relacionadas con el modelo y el controlador
import models.Main;
import models.Usuario;
import mvc.Controlador;
import mvc.Vista;
import usuario.views.LoginView;
import usuario.views.LoginViewBuilder;
import usuario.views.RegisterView;
import usuario.views.RegisterViewBuilder;
import usuario.views.UsuarioViewBuilder;
import usuario.views.Views;
import controllers.MainController;
import controllers.UsuarioController;

// Definici�n de la clase MainView que extiende de Vista
public class MainView extends Vista<Main> {

	// Atributo para la ventana principal
	private JFrame frame;

	// Constructor de la clase
	public MainView() {
		initialize(); // Llamada al m�todo para inicializar la interfaz
	}

	// M�todo privado para inicializar la interfaz
	private void initialize() {
		// Creaci�n de la ventana principal
		frame = new JFrame();
		frame.getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		frame.setBounds(100, 100, 482, 350); // Establecimiento de las dimensiones de la ventana
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Definici�n de la operaci�n al cerrar la ventana
		frame.setVisible(true); // Hacer visible la ventana

		// Creaci�n de un objeto de la clase Usuario
		Usuario u = new Usuario();

		// Creaci�n de una vista de login y registro
		LoginView login = Views.login(u, new UsuarioController(u));
		RegisterView register = Views.register(u, (UsuarioController) u.getControlador());

		// Agregar los componentes a la ventana principal
		frame.getContentPane().add(register);
		frame.getContentPane().add(login);

		frame.pack(); // Ajustar el tama�o de la ventana autom�ticamente
	}
}
